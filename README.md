# Dyslexic error analyzer

[Scroll down to English](#presentation)

## Présentation

Ce travail a été réalisé dans le cadre de la thèse de Johana Bodard au sein du [laboratoire CHArt](http://laboratoire-chart.fr/) de l'[université Paris 8](https://www.univ-paris8.fr/).

L'objectif de ce travail est d'analyser les erreurs d'orthographe et de grammaire d'un corpus de textes rédigés par des personnes dyslexiques françaises.
Ce corpus, appelé *Corpus DYS*, est disponible sur [Nakala](https://nakala.fr/10.34847/nkl.ced0370u).

Ce travail peut également être utilisé pour analyser les erreurs d'autres corpus, du moment que celles-ci ont été annotées en suivant le [guide d'annotation](https://nakala.fr/10.34847/nkl.dabbn552) du *Corpus DYS*.

## Contenu

- *dyslexic_error_analyzer.ipynb* : un notebook Jupyter Python qui permet d'analyser les erreurs extraites d'un corpus de textes ; 
- *getting_started_fr.pdf* : un document expliquant comment installer un environnement de développement pour exécuter le notebook ;
- *README.md* : le présent fichier.

## Presentation

This work was carried out as part of Johana Bodard's PhD thesis in the [CHArt laboratory](http://laboratoire-chart.fr/) at the [Paris 8 University](https://www.univ-paris8.fr/).

The objectif of this work is to analyze spelling and grammar errors in a a corpus of texts written by French people with dyslexia.
This corpus, nammed *Corpus DYS*, is available on [Nakala](https://nakala.fr/10.34847/nkl.ced0370u).

This work can also be used to analyze errors in other corpora, as long as the errors have been annotated following the [annotation guide](https://nakala.fr/10.34847/nkl.dabbn552) of *Corpus DYS*.

## Content

- *dyslexic_error_analyzer.ipynb*: a Jupyter Python notebook for analyzing errors extracted from a corpus of texts; 
- *getting_started_fr.pdf*: a guide explaining how to set up a development environment to run the notebook;
- *README.md*: the current file.




